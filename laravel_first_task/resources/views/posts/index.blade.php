@extends('layout.app')

@section('content')
<h1>Posts</h1> 

@if (count($posts) > 0)
            
                @foreach ($posts as $post)

                <div class="card bg-light text-dark">
                    <div class="card-header "><h3><a href="/posts/{{$post->id}}"> {{$post->title}} </a></h3></div>
                    <div class="card-footer"><small>written on {{$post->created_at}}</small></div>
                </div>
                <br>
                @endforeach
                {{$posts->links()}}
        @endif

@endsection